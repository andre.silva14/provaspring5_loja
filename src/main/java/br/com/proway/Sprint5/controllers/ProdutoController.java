package br.com.proway.Sprint5.controllers;

import br.com.proway.Sprint5.models.Produto;
import br.com.proway.Sprint5.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class ProdutoController {

    @Autowired
    ProdutoRepository pRepository;

    @GetMapping("/produtos")
    public ArrayList<Produto> getEstoque(){
        return pRepository.findAll();
    }

    @GetMapping("/produto/{id}")
    public Produto getProduto(@PathVariable("id") long id){
        return pRepository.findById(id);
    }

    @DeleteMapping("/produto/{id}")
    void deleteProduto(@PathVariable("id") long id){
        pRepository.deleteById(id);
    }

    @PostMapping("/produto/add")
    public ResponseEntity addProduto(@RequestBody Produto produto){
        Produto p;
        try {
            p = pRepository.save(produto);
            return new ResponseEntity<>(p, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/produto/{id}")
    public Produto updateProduto(@PathVariable("id") long id, @RequestBody Produto produto){
        produto.setId(id);
        return pRepository.save(produto);
    }

    /** Busca os produtos com quantidade abaixo de 5 **/
    @GetMapping("/produto/esgotando")
    public ArrayList<Produto> getProdutoAcabando(){
        return pRepository.getProdutoAcabando();
    }




}

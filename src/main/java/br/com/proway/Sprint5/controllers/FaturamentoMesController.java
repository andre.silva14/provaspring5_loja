package br.com.proway.Sprint5.controllers;

import br.com.proway.Sprint5.models.FaturamentoMes;
import br.com.proway.Sprint5.repository.FaturamentoMesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class FaturamentoMesController {

    @Autowired
    FaturamentoMesRepository fmRepository;

    @GetMapping("/meses")
    public ArrayList<FaturamentoMes> getMeses(){ return fmRepository.findAll();}

    @GetMapping("/mes/{id}")
    public FaturamentoMes getMes(@PathVariable("id") long id){ return fmRepository.findById(id);}

    @DeleteMapping("/mes/{id}")
    void deleteMes(@PathVariable("id") long id){ fmRepository.deleteById(id);}

    /** Adiciona mês **/
    @PostMapping("/mes/add")
    public ResponseEntity addMes(@RequestBody FaturamentoMes faturamentoMes){
        FaturamentoMes fm;
        try {
            fm = fmRepository.save(faturamentoMes);
            return new ResponseEntity<>(fm, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/mes/{id}")
    public FaturamentoMes updateMes(@PathVariable("id") long id, @RequestBody FaturamentoMes faturamentoMes){
        faturamentoMes.setId(id);
        return fmRepository.save(faturamentoMes);
    }

    /** Retorna mês que lucrou menos que a média  **/
    @GetMapping("/mes/negativo")
    public ArrayList<FaturamentoMes> getMesNegativo(){
        return fmRepository.mesNegativo();
    }
}

package br.com.proway.Sprint5.controllers;

import br.com.proway.Sprint5.models.Funcionario;
import br.com.proway.Sprint5.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class FuncionarioController {

    @Autowired
    FuncionarioRepository funRepository;

    @GetMapping("/funcionarios") /*  */
    public ArrayList<Funcionario> getFuncionarios(){return funRepository.findAll();}

    @GetMapping("/funcionario/{id}")
    public Funcionario getFuncionario(@PathVariable("id") long id){ return funRepository.findById(id);}

    @DeleteMapping("/funcionario/{id}")
    void deleteFuncionario(@PathVariable("id") long id){funRepository.deleteById(id);}

    @PostMapping("funcionario/add")
    public ResponseEntity addFuncionario(@RequestBody Funcionario funcionario){
        Funcionario fun;
        try {
            fun = funRepository.save(funcionario);
            return new ResponseEntity<>(fun, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/funcionario/{id}")
    public Funcionario updateFuncionario(@PathVariable("id") long id, @RequestBody Funcionario funcionario){
        funcionario.setId(id);
        return funRepository.save(funcionario);
    }

    /** Pega funcionarios com salario acima da média **/
    @GetMapping("/funcionario/salario")
    public ArrayList<Funcionario> salarioMaiorMedia(){
        return funRepository.salarioAcimaMedia();
    };
}

package br.com.proway.Sprint5.repository;

import br.com.proway.Sprint5.models.FaturamentoMes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface FaturamentoMesRepository extends JpaRepository<FaturamentoMes, Long> {

    ArrayList<FaturamentoMes> findAll();

    FaturamentoMes save(FaturamentoMes faturamentoMes);

    FaturamentoMes findById(long id);

    void deleteById(long id);

    @Query("SELECT fm FROM FaturamentoMes fm  WHERE lucro  < (SELECT AVG(lucro) FROM FaturamentoMes )")
    ArrayList<FaturamentoMes> mesNegativo();
}

package br.com.proway.Sprint5.repository;

import br.com.proway.Sprint5.models.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
    ArrayList<Funcionario> findAll();

    Funcionario findById(long id);


    void deleteById(long id);

    Funcionario save(Funcionario funcionario);

    @Query("SELECT f FROM Funcionario f WHERE salario > (SELECT AVG(salario) FROM Funcionario)")
    ArrayList<Funcionario> salarioAcimaMedia();

}

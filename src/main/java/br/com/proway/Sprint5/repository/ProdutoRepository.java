package br.com.proway.Sprint5.repository;

import br.com.proway.Sprint5.models.Funcionario;
import br.com.proway.Sprint5.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Optional;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    ArrayList<Produto> findAll();

    Produto save(Produto produto);

    Produto findById(long id);

    void deleteById(long id);

    @Query("select p from Produto p where p.quantidade <= 5")
    ArrayList<Produto> getProdutoAcabando();
}

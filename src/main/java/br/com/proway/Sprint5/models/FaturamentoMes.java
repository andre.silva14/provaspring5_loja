package br.com.proway.Sprint5.models;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "faturamento_mes")
public class FaturamentoMes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "mes")
    private String mes;
    @Column(name = "lucro")
    private double lucro;
}

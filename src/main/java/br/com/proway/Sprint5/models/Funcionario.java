package br.com.proway.Sprint5.models;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "funcionario")
public class Funcionario {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
   @Column(name = "nome")
    private String nome;
   @Column(name = "salario")
    private double salario;
}
